# Poradnik uruchomieniowy aplikacji ChatLord dla potrzeb lokalnych & deweloperskich

Autor: Gabriel Górski

## Wymagania

- Zainstalowany pakiet **.NET Core 2.2 SDK**

Wynik poniższej komendy powinien zapewniać o poprawności instalacji

    PS C:> dotnet --info
    .NET Core SDK (reflecting any global.json):
    Version:   2.2.203
    Commit:    e5bab63eca

    Runtime Environment:
    OS Name:     Windows
    OS Version:  10.0.17763
    OS Platform: Windows
    RID:         win10-x64
    Base Path:   C:\Program Files\dotnet\sdk\2.2.203\

    Host (useful for support):
    Version: 2.2.4
    Commit:  f95848e524

    .NET Core SDKs installed:
    2.2.203 [C:\Program Files\dotnet\sdk]

    .NET Core runtimes installed:
    Microsoft.AspNetCore.All 2.2.4 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.All]
    Microsoft.AspNetCore.App 2.2.4 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
    Microsoft.NETCore.App 2.2.4 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]

    To install additional .NET Core runtimes or SDKs:
    https://aka.ms/dotnet-download

- Zainstalowany node.js

Wynik poniższej komendy powinien zapewniać o poprawności instalacji

    PS C:> npm --version
    6.4.1

## Uruchomienie

Następnie należy kolejno

1 | Wejść do katalogu ZtiProjectServer i wykonać polecenie

    $ dotnet run --project src/WebApi

2 | W osobnej powłoce wejść do katalogu ZtiProjectClient i wykonać polecenie

    $ npm install

3 | Po zainstalowaniu się wszystkich dependencji należy wykonać komendę

    $ npm start

W ten sposób aplikacja webowa uruchomi się w trybie developerskim.

4 | Otwórz link do aplikacji webowej który się wyświetlił w konsoli

### Prod

Dla potrzeb produkcyjnych można opublikować wynik budowania i hostować aplikację na wybranych serwerze HTTP:

Komenda z katalogu ZtiProjectClient

    $ ng build

Kod HTML, JS i CSS oraz inne konieczne do hostowania aplikacji zasoby znajdują się w katalogu ZtiProjectClient/dist

## Podsumowanie

Przy hostowaniu aplikacji należy pamiętać o odpowiednim zadbaniu o konfigurację CORSa który przy przesyłaniu **Credentials** nie może być **AnyOrigin** oraz domen

- Zmienna **ClientUrls** w pliku appsettings.json w ZtiProjectServer/src/WebApi/appsettings.json
- Zmienna **backendUri** w odpowiednich plikach **Environment** w katalogu ZtiProjectClient/src/environments/
