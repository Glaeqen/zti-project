# Opis funkcjonalny aplikacji ChatLord

Autor: Gabriel Górski

## Cel aplikacji

Aplikacja umożliwia komunikację z innymi zalogowanymi użytkownikami na zasadach podobnych jak ma to miejsce w usługach pokroju IRC, Discord czy Slack z odpowiednio uproszczonymi funkcjonalnościami. Pozwala na tworzenie grup, dołączanie do odpowiednio już istniejących i rozmowy tekstowe z ich członkami.

## Podział

Aplikacja składa się z dwóch większych komponentów:

- aplikacji webowej stworzonej w środowisku **Angular 8** (w dalszej części nazywanej _frontend'em_) - ZtiProjectClient
- aplikacji serwerowej opartej o technologię **ASP.NET Core 2.2** (w dalszej części nazywanej _backend'em_) - ZtiProjectServer.

a dodatkowo znajduję się w nim katalog **docs** w którym znajduje się ta dokumentacja techniczna.

### Frontend

#### Zastosowane technologie

Najważniejsze filary składowe stanowią:

- środowisko **Angular 8**
- biblioteka **rxJS** - integrująca się z Angularem biblioteka pozwalająca na pisanie w ramach paradygmatu reaktywnego
- biblioteka **@aspnet/signalr.js** - klient JS środowiska **SignalR** do komunikacji RPC
- system stylowania **Bootstrap 4**

#### Struktura aplikacji

Na aplikację składa się 6 komponentów

- **Authentication**
  - **SignIn**
  - **SignUp**
- **Chat**
- **Header**
- **PageNotFound**

oraz kilka abstrakcji pomocniczych w postaci

- **AuthTokenInterceptor** - wstrzykujący tokeny autoryzujące **JWT** do każdego zapytania XHR.
- **AuthenticationGuard** oraz **AlreadyAuthenticatedGuard** - obiekty uniemożliwiające _route-owanie_ przeglądarki użytkownika na ścieżki zakazane
- **ChatHubMessageManager** - menedżer wiadomości
- Liczne modele i DTO np. **MessageView**

### Diagram UML

![Diagram UML dla frontend'u](diagrams/uml_frontend.svg)

### Backend

#### Najważniejsze technologie

- środowisko **.NET Core 2.2**
  - WebAPI realizowane za pomocą **ASP.NET Core 2.2**
  - Komunikacja dynamiczna klient-serwer za pomocą środowiska **SignalR**
  - Mapowanie ORM za pomocą **Entity Framework Core 2.2**

#### Interfejs publiczny

Aplikacja udostępnia dwa **endpoint'y** publiczne w ramach usługi **REST**

##### Uwierzytelnianie użytkownika

- **URL** >> /api/auth/sign-in
- **Metoda** >> POST
- **Dane wejściowe** >> application/json
  - 'username': string
  - 'password': string
    - pole musi być w postaci **base64**
- **Rezultat zwracany** >> application/json
  - 'token': string
    - Token **JWT** podpisany przez aplikację z czasem wygaśnięcia 1 dzień.

##### Rejestracja użytkownika

- **URL** >> /api/auth/sign-up
- **Metoda** >> POST
- **Dane wejściowe** >> application/json
  - 'username': string
  - 'email': string
  - 'password': string
    - pole musi być w postaci **base64**
- **Rezultat zwracany** >> application/json
  - 'message': string

##### Chat

Oprócz tego aplikacja udostępnia 1 **endpoint** w ramach środowiska **SignalR**

- **URL** >> /api/chat

#### Struktura aplikacji

Aplikacja jest podzielona na 6 podprojektów głównych oraz podprojekty testujące realizując w ten sposób standardowe konwencje architektur **.NET** jeśli chodzi o strukturę i częściowo czerpie z rozwiązań **Domain Driven Design**.

- **Application** - projekt realizujący logikę biznesową aplikacji m.in.
  - **Operations** - implementacje logiki na użytek kontrolerów WebAPI
  - **Services** - implementacje logiki na użytek kontrolera SignalR (tzw. _hub_)
- **Concepts** - wysokopoziomowe abstrakcje wykorzystywane do implementacji rozwiązań. Między innymi np. **Operations**
- **Contracts** - zbiór kontraktów między infrastrukturą a logiką biznesową, m.in. wyjątki, modele, a także kontrakty repozytoriów i serwisów.
- **Infrastructure** - projekt przechowujący tzw. infrastrukturę aplikacji tj. narzędzia nie związane bezpośrednio z obiektami domeny biznesowej, ale stanowiącą filar działania komponentów realizujących logikę biznesową
- **Persistence** - element infrastruktury, który ze względu na specyfikę swego działania stanowi osobny projekt. Przechowuje modele encji i komponenty konieczne do interakcji z bazą danych.
- **WebApi** - projekt stanowiący punkt wejścia aplikacji. Implementuje wymagane przez środowisko **ASP.NET Core** interfejsy konieczne do realizacji **IoC**, rejestruje klasy w kontenerze **DI**, startuje aplikację, wystawia **endpoint'y** i deleguje realizację zadań do innych projektów.

#### Diagram komponentów

![Diagram komponentów dla backend'u](diagrams/uml_backend.png)

#### SignalR

SignalR jest biblioteką służącą do realizacji komunikacji klient-serwer na zasadach **RPC** tj. **Remote Procedure Call**.

Biblioteka jest technologicznie agnostyczna, co oznacza że potrafi korzystać z protokołu **WebSocket**, **SSE** lub w ostateczności z tzw. **long pollingu** jeśli poprzednie metody okażą się niedostępne.

Klient korzystając z biblioteki potrafi wołać metody udostępnione po stronie serwera i vice versa. W przypadku aplikacji **ChatLord** pozwoliło to na stworzenie responsywnego, dynamicznego systemu zarządzania użytkownikami w grupach oraz przesyłanie wiadomości.

#### Persystencja

Dane o użytkownikach przechowywane są bazie **PostgreSQL** i przechowywane są w tabeli **users** o sygnaturze

- id : int : klucz główny
- username : varchar(255) : not null : unique
- email : varchar(255) : not null : unique
- hashedpassword : varchar(255) : not null

Po wykonaniu mapowania obiektowo relacyjnego, tworzony jest obiekt **UserEntity** w postaci

    public partial class UserEntity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Hashedpassword { get; set; }
    }

W projekcie wykorzystywany jest **PostgreSQL** jako **SaaS** w usłudze **ElephantSQL**.

## Bezpieczeństwo

### Tokeny JWT

Aplikacja wykorzystuje tokeny JWT tylko i wyłącznie w celach uwierzytelniania i waliduje ich poprawność w przypadku każdego zapytania - także jeśli chodzi o SignalR. Mechanizmy po stronie serwera są natywnie wspierany ze strony środowiska **ASP.NET Core**, natomiast po stronie klienta realizowane są przez tzw. _guard'y_ oraz _interceptory_. Tak więc **backend** pełni również rolę serwera autoryzującego.

### Hasła

#### Base64

Hasła z frontend'u do backend'u przesyłane są w formie **base64** by utrudnić przypadkowe przeczytanie hasła przez osoby trzecie i nie stanowi żadnej dodatkowej ochrony.

#### Baza danych

Hasła przed zapisaniem do bazy danych są **hash'owane** za pomocą algorytmu **SHA256** utrudniając w ten sposób możliwość szkodliwego wykorzystania haseł w razie potencjalnego wycieku danych.

## Pozostałe diagramy

### Diagram sekwencji RPC

- diagram sekwencji dla uwierzytelniania

![s](diagrams/uml_seq_auth.png)

- diagram sekwencji dla komunikacji RPC

![s](diagrams/uml_seq_chat.png)

## Testowanie

Aby uruchomić testy jednostkowe, należy w katalogu z projektem .NET zastosować komendę

    $ dotnet test
