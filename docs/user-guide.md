# Podręcznik użytkownika aplikacji ChatLord w języku polskim

Autor: Gabriel Górski

## Cel aplikacji

Aplikacja umożliwia komunikację z innymi zalogowanymi użytkownikami na zasadach podobnych jak ma to miejsce w usługach pokroju IRC, Discord czy Slack z odpowiednio uproszczonymi funkcjonalnościami. Pozwala na tworzenie grup, dołączanie do odpowiednio już istniejących i rozmowy tekstowe z ich członkami.

## Układ aplikacji

Aplikacja posiada standardową budowę. Przestrzeń pracy użytkownika składa się z paska nawigującego po komponentach aplikacji i miejsca na sam komponent.

### Ekran logowania

Po wybraniu z paska opcji **Login | Sign up**, użytkownik zostanie przeniesiony na stronę logowania/rejestracji.

![Ekran logowania](auth_page.png)

#### Logowanie

Po lewej stronie widoczne są pola tekstowe umożliwiające użytkownikowi podanie swojego loginu i hasła (pola **username** i **password**). Po udanej próbie uwierzytelniania, użytkownik zostanie przekierowany do zasadniczej części aplikacji.

#### Rejestracja

Po prawej stronie widoczne są pola tekstowe umożliwiające użytkownikowi podanie swojego loginu, e-maila oraz hasła w celu rejestracji. Po udanej próbie rejestracji, użytkownik zostanie automatycznie zalogowany i przekierowany do zasadniczej części aplikacji.

### Ekran komunikatora tekstowego (_chat_)

![Ekran chatu](chat_page.png)

Po poprawnym zalogowaniu, użytkownik uzyska dostęp do komunikatora tekstowego (określanego od tego momentu podręcznika _chatem_).

#### Grupy (_alias pokoje_)

W oknie o nazwie **Groups** znajdują się wylistowane wszystkie dostępne grupy do których użytkownik może się zasubskrybować przyciskiem **Join** lub jeśli jest już członkiem jednej z nich - odsubskrybować przyciskiem **Leave**.

##### Dodawanie nowej grupy

W polu tekstowym **Add new group** możemy wpisać nazwę grupy którą chcielibyśmy stworzyć, a następnie wcisnąć _Enter_. Nowa grupa pojawi się na liście i będzie automatycznie zasubskrybowana oraz wybrana przez tworzącego ją użytkownika.

##### Usuwanie grup

Nie ma możliwości jawnego usunięcia grupy. Grupa jest automatycznie usuwana w momencie gdy odsubskrybuje się ostatni jej członek.

##### Wybieranie grupy

Grupa jest automatycznie wybrana zaraz po jej stworzeniu lub zasubskrybowaniu. Ponadto użytkownik może manualnie przełączać się między zasubskrybowanymi grupami klikając na interesujący pokój.

##### Persystencja

Grupy są zapisywane, co pozwala użytkownikowi wylogowanie się bez utraty wybranych kanałów. Wiadomości (podobnie jak ma to miejsce w przypadku IRC) są ulotnie przechowywane w przeglądarce.

#### Komunikacja tekstowa

Po prawej stronie znajduje się zasadnicze okno chatu. Wiadomości użytkownika pojawiają się po prawej stronie i oznaczane są kolorem niebieskim. Wiadomości innych uczestników chatu pojawiają się po lewej stronie i oznaczane są kolorem szarym. Aby wysłać wiadomość należy wypełnić pole tekstowe i wcisnąć **Enter**.

## Zakończenie pracy z aplikacją

Za pomocą przycisku **Logout <nazwa użytkownika>** użytkownik może wylogować się z aktualnie uczęszczanej sesji. Jeśli użytkownik zamknie przeglądarkę bez wylogowywania, po ponownym uruchomieniu aplikacja będzie pamiętała ostatnio zalogowanego użytkownika.
