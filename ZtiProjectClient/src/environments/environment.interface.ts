export interface EnvironmentInterface {
  backendUri: string;
  production: boolean;
}
