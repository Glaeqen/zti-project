import {EnvironmentInterface} from './environment.interface';

export const environment: EnvironmentInterface = {
  backendUri: 'http://10.0.0.104:5000',
  production: true
};
