import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from "../services/authentication.service";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthenticated: boolean;
  private userSubscription: Subscription;

  constructor(private authService: AuthenticationService, private router: Router) {}

  onLogOut() {
    this.authService.signOut();
  }

  ngOnInit() {
    this.userSubscription = this.authService.UserChanged.subscribe(user => {
      this.isAuthenticated = user.loggedIn;
      if(!this.isAuthenticated){
        this.authService.redirectUrl = this.router.url;
        this.router.navigate(['authenticate']);
      }
    })
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

}
