export interface MessageView {
  user: string;
  body: string;
  date: Date;
}