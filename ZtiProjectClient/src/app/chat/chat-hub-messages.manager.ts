import {MessageView} from "./models/message-view";

export class ChatHubMessagesManager {

  private container: Map<string, Array<MessageView>> = new Map<string, Array<MessageView>>();

  AddMessage(group: string, message: MessageView): void {
    if (this.container.has(group)) {
      this.container.get(group).push(message);
    } else {
      this.container.set(group, new Array<MessageView>(message));
    }
  }

  GetMessages(group: string): Array<MessageView> {
    return this.container.has(group) ? this.container.get(group) : new Array<MessageView>();
  }

  ClearMessages(group: string): void {
    this.container.delete(group);
  }
}
