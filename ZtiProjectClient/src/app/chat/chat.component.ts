import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr'
import {AuthenticationService} from "../services/authentication.service";
import {environment} from "../../environments/environment";
import {BehaviorSubject, Subscription} from "rxjs";
import {Message} from "./models/message";
import {MessageView} from "./models/message-view";
import {ChatHubMessagesManager} from "./chat-hub-messages.manager";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {
  public static readonly LAST_PICKED_GROUP_TOKEN = "lastPickedGroup";

  @ViewChild('messageList') messageList: ElementRef<HTMLDivElement>;

  private connection: HubConnection;
  private messagesManager: ChatHubMessagesManager = new ChatHubMessagesManager();
  private selectedGroup: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private selectedGroupSubscription: Subscription;

  selectedGroupAsValue: string = this.selectedGroup.getValue();
  subscribedGroups: string[] = [];
  groups: string[] = [];
  messages: Array<MessageView>;

  constructor(private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.selectedGroupSubscription = this.selectedGroup.subscribe(newSelectedGroup => {
      console.log("newSelectedGroup triggered: " + newSelectedGroup);
      this.selectedGroupAsValue = newSelectedGroup;
      this.messages = this.messagesManager.GetMessages(newSelectedGroup);
    });

    this.connection = new HubConnectionBuilder().withUrl(environment.backendUri + "/api/chat",
      {accessTokenFactory: () => this.authService.UserChanged.getValue().accessToken}
    ).build();

    this.connection.on("ReceiveMessageForGroup", (message: Message, group: string) => {
      console.log("ReceiveMessageForGroup event came:\nmessage: " + JSON.stringify(message) + "\ngroup: " + group);
      this.messagesManager.AddMessage(group, {
        user: message.user,
        body: message.body,
        date: new Date()
      } as MessageView);
      if(this.selectedGroup.getValue() === group){
        this.messages = this.messagesManager.GetMessages(group);
      }
    });

    this.connection.on("GroupsChangedEvent", () => {
      console.log("GroupsChangedEvent event came");
      this.connection.invoke("GetAllGroups")
        .then(groups => {
          this.groups = groups;
          console.log("Groups updated: " + groups);
        })
        .catch(err => console.error(err.toString()));

    });

    this.connection.on("SubscriptionsChangedEvent", () => {
      console.log("SubscriptionsChangedEvent event came");
      this.connection.invoke("GetGroupsForUser", this.authService.UserChanged.getValue().username)
        .then(groups => {
          this.subscribedGroups = groups;
          console.log("SubscribedGroups updated: " + groups);
          if(!this.subscribedGroups.includes(this.selectedGroup.getValue())) {
            this.selectedGroup.next(null);
          }
        })
        .catch(err => console.error(err.toString()));

    });

    this.connection.start()
      .then(() => {
        this.connection.invoke("GetAllGroups")
          .then(groups => {
            this.groups = groups;
            console.log("Groups updated: " + groups);
          })
          .catch(err => console.error(err.toString()));
        ;
        this.connection.invoke("GetGroupsForUser", this.authService.UserChanged.getValue().username)
          .then(groups => {
            this.subscribedGroups = groups;
            console.log("SubscribedGroups updated: " + groups);
          })
          .catch(err => console.error(err.toString()));
        ;
      })
      .catch(err => console.error(err.toString()));

  }

  ngOnDestroy(): void {
    this.selectedGroupSubscription.unsubscribe();
    this.connection.stop().catch(err => console.error(err.toString()))
  }

  sendMessage(body: string) {
    if(body.replace(/^\s+|\s+$/g, '').length === 0) return
    const user: string = this.authService.UserChanged.getValue().username;
    console.log("sendMessage triggered:\nmessage: " + body + "\nfrom: " + user + "\nto group: " + this.selectedGroup.getValue());
    const selectedGroup = this.selectedGroup.getValue();
    if (!selectedGroup) {
      return;
    }

    this.connection
      .invoke(
        "SendMessageToGroup",
        {user, body} as Message, selectedGroup
      )
      .catch(err => console.error(err.toString()));
  }

  onGroupSelected(group: string) {
    this.selectedGroup.next(group);
  }

  private addMeToGroup(group: string) {
    console.log("addMeToGroup " + group + " triggered");
    this.connection
      .invoke("AddMeToGroup", group)
      .then(() => {
        this.selectedGroup.next(group);
      })
      .catch(err => console.error(err.toString()))
  }

  private removeMeFromGroup(group: string) {
    console.log("removeMeFromGroup " + group + " triggered");
    this.connection
      .invoke("RemoveMeFromGroup", group)
      .then(() => {
      })
      .catch(err => console.error(err.toString()))
  }

  onGroupSubscription(group: string) {
    console.log("onGroupSubscription " + group + " triggered");
    this.addMeToGroup(group);
  }

  onGroupUnsubscription(group: string) {
    console.log("onGroupUnsubscription " + group + " triggered");
    this.removeMeFromGroup(group);
  }
}
