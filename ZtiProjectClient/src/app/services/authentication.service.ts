import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Router} from "@angular/router";
import {JwtHelperService} from "@auth0/angular-jwt"

export interface SignInUserCredentials {
  username: string;
  password: string;
}

export interface SignUpUserCredentials {
  username: string;
  email: string;
  password: string;
}

export interface Token {
  token: string;
}

export interface User {
  loggedIn: boolean;
  accessToken?: string;
  username?: string;
}

export abstract class HttpServiceToBackendBase {
  protected readonly baseUrl: string;

  constructor() {
    this.baseUrl = environment.backendUri;
  }
}

@Injectable()
export class AuthenticationService extends HttpServiceToBackendBase {
  public static readonly SESSION_STORAGE_TOKEN = 'accessToken';
  private readonly authenticationUrl: string = this.baseUrl + '/api/auth';

  public readonly UserChanged: BehaviorSubject<User> = new BehaviorSubject<User>({
    loggedIn: false
  });

  public redirectUrl: string = null;

  constructor(private httpClient: HttpClient, private router: Router) {
    super();
    const tokenFromStorage: string = localStorage.getItem(AuthenticationService.SESSION_STORAGE_TOKEN);
    if (tokenFromStorage) {
      this.UserChanged.next({
        accessToken: tokenFromStorage,
        loggedIn: true,
        username: new JwtHelperService().decodeToken(tokenFromStorage).subject
      } as User);
    }
  }

  public signIn(credentials: SignInUserCredentials): void {
    credentials.password = btoa(credentials.password);

    this.httpClient.post<Token>(
      this.authenticationUrl + "/sign-in",
      credentials,
    ).subscribe((token: Token) => {
      this.UserChanged.next({
        accessToken: token.token,
        loggedIn: true,
        username: credentials.username
      } as User);
      localStorage.setItem(AuthenticationService.SESSION_STORAGE_TOKEN, token.token);
      if (this.redirectUrl) {
        this.router.navigate([this.redirectUrl]);
        this.redirectUrl = null;
      } else {
        this.router.navigate(['/']);
      }
    }, error => {
      this.UserChanged.next({
        loggedIn: false
      } as User);
    });
  }

  public signUp(credentials: SignUpUserCredentials): Observable<string> {
    if (this.UserChanged.getValue().loggedIn) {
    }
    credentials.password = btoa(credentials.password);
    return this.httpClient.post<string>(
      this.authenticationUrl + "/sign-up",
      credentials,
    );
  }

  public signOut(): void {
    localStorage.removeItem(AuthenticationService.SESSION_STORAGE_TOKEN);
    this.UserChanged.next({
      loggedIn: false
    });
  }

}
