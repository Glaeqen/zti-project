export interface HttpRequestFailure {
  message: string,
  statusCode: number
}