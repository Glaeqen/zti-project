import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {
  AuthenticationService,
  SignInUserCredentials,
  SignUpUserCredentials
} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {HttpRequestFailure} from "../models/http-request-failure";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit, OnDestroy {

  signInForm: FormGroup;
  private onSubmitClicked: boolean = false;
  private registrationFailed: boolean = false;
  private registrationSuccess: boolean = false;
  private registrationFailedReason: HttpRequestFailure;

  constructor(private authService: AuthenticationService, private router: Router) {

  }

  ngOnInit() {
    this.signInForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required]),
        'email': new FormControl(null, [Validators.required, Validators.email]),
        'password': new FormControl(null, [Validators.required]),
        'repeatPassword': new FormControl(null, [Validators.required])
      }, [SignUpComponent.checkPasswords]),
    });
  }

  onSubmit() {
    const username = this.signInForm.value.userData.username;
    const password = this.signInForm.value.userData.password;
    this.onSubmitClicked = true;
    this.authService.signUp({
      username,
      email: this.signInForm.value.userData.email,
      password
    } as SignUpUserCredentials)
      .subscribe(
        success => {
          this.registrationSuccess = true;
          this.authService.signIn({username, password} as SignInUserCredentials);
        },
        error => {
          this.registrationFailed = true;
          this.registrationFailedReason = {
            message: error.error.Message,
            statusCode: error.status
          } as HttpRequestFailure
          });
  }

  ngOnDestroy(): void {
  }

  static checkPasswords(group: FormGroup) {
    const pass = group.get('password').value;
    const confirmPass = group.get('repeatPassword').value;

    return pass === confirmPass ? null : {notSamePasswords: true};
  }
}
