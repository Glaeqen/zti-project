import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService, SignInUserCredentials, User} from "../../services/authentication.service";
import {Observable, Subscription} from "rxjs";
import {nextContext} from "@angular/core/src/render3";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit, OnDestroy {
  signInForm: FormGroup;
  private onSubmitClicked: boolean = false;
  private authenticationFailed: boolean = false;
  private authenticationSuccess: boolean = false;
  private userChangedSubscription: Subscription;

  constructor(private authService: AuthenticationService, private router: Router) {

  }

  ngOnInit() {
    this.signInForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required]),
        'password': new FormControl(null, [Validators.required])
      }),
    });
    this.userChangedSubscription = this.authService.UserChanged.subscribe(next => {
      this.authenticationFailed = this.onSubmitClicked && !next.loggedIn;
      this.authenticationSuccess = this.onSubmitClicked && next.loggedIn;
    })
  }

  onSubmit() {
    this.authService.signIn({
      username: this.signInForm.value.userData.username,
      password: this.signInForm.value.userData.password
    } as SignInUserCredentials);
    this.onSubmitClicked = true;
  }

  ngOnDestroy(): void {
    this.userChangedSubscription.unsubscribe();
  }

}
