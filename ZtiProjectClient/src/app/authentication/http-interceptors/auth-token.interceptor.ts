import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor{

  constructor(private authService: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this.authService.UserChanged.getValue();
    if(user.accessToken){
      req.headers.append("Authorization", ["Bearer", user.accessToken])
    }
    return next.handle(req);
  }

}
