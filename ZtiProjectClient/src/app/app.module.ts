import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {ChatComponent} from './chat/chat.component';
import {AuthenticationComponent} from './authentication/authentication.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {HeaderComponent} from './header/header.component';
import {AuthenticationService} from './services/authentication.service';
import {HttpClientModule} from '@angular/common/http';
import { SignInComponent } from './authentication/sign-in/sign-in.component';
import { SignUpComponent } from './authentication/sign-up/sign-up.component';
import {AlreadyAuthenticatedGuard, AuthenticationGuard} from "./authentication/authentication.guard";

const ROUTES: Routes = [
  {path: '', redirectTo: 'chat', pathMatch: 'full'},
  {path: 'chat', component: ChatComponent, canActivate: [AuthenticationGuard]},
  {path: 'authenticate', component: AuthenticationComponent, canActivate: [AlreadyAuthenticatedGuard]},
  {path: 'not-found', component: PageNotFoundComponent},
  {path: '**', redirectTo: 'not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    AuthenticationComponent,
    PageNotFoundComponent,
    HeaderComponent,
    SignInComponent,
    SignUpComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
