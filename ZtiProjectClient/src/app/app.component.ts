import {Component, ViewChild} from '@angular/core';
import {OnInit} from '@angular/core/src/metadata/lifecycle_hooks';
import {AuthenticationService} from "./services/authentication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ZtiProjectClient';

  constructor(private authService: AuthenticationService){}

  ngOnInit(): void {
    this.authService.UserChanged.subscribe(user => {
      if(!user.loggedIn){

      }
    })
  }
}
