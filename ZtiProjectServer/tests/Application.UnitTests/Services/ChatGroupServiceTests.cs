using System.Collections.Generic;
using FluentAssertions;
using Microsoft.EntityFrameworkCore.Query.ExpressionTranslators.Internal;
using Xunit;
using ZtiProjectServer.Application.Services;

namespace Application.UnitTests.Services
{
    public class ChatGroupServiceTests
    {
        private readonly ChatGroupService _chatGroupService;

        public ChatGroupServiceTests()
        {
            _chatGroupService = new ChatGroupService();
            _chatGroupService.AddUserToGroup("u1", "g1");
            _chatGroupService.AddUserToGroup("u2", "g1");
            _chatGroupService.AddUserToGroup("u2", "g2");
            _chatGroupService.AddUserToGroup("u3", "g1");
            _chatGroupService.AddUserToGroup("u3", "g2");
            _chatGroupService.AddUserToGroup("u3", "g3");
        }
        
        [Fact]
        public void ShouldProperlyGetGroupsWithPredicates()
        {
            var groupsAll = _chatGroupService.GetGroups(_ => true);
            var groupsWithUsersEndingWith1 = _chatGroupService.GetGroups(user => user.EndsWith("1"));
            var groupsWithUsersEndingWith2 = _chatGroupService.GetGroups(user => user.EndsWith("2"));
            var groupsWithUsersEndingWith3 = _chatGroupService.GetGroups(user => user.EndsWith("3"));

            groupsAll.Should().BeEquivalentTo(new HashSet<string> {"g1", "g2", "g3"});
            groupsWithUsersEndingWith1.Should().BeEquivalentTo(new HashSet<string> {"g1"});
            groupsWithUsersEndingWith2.Should().BeEquivalentTo(new HashSet<string> {"g1", "g2"});
            groupsWithUsersEndingWith3.Should().BeEquivalentTo(new HashSet<string> {"g1", "g2", "g3"});
        }
        
        [Fact]
        public void ShouldProperlyGetUsersWithPredicates()
        {
            var usersAll = _chatGroupService.GetUsers(_ => true);
            var usersInGroupsEndingWith1 = _chatGroupService.GetUsers(group => group.EndsWith("1"));
            var usersInGroupsEndingWith2 = _chatGroupService.GetUsers(group => group.EndsWith("2"));
            var usersInGroupsEndingWith3 = _chatGroupService.GetUsers(group => group.EndsWith("3"));

            usersAll.Should().BeEquivalentTo(new HashSet<string> {"u1", "u2", "u3"});
            usersInGroupsEndingWith1.Should().BeEquivalentTo(new HashSet<string> {"u1", "u2", "u3"});
            usersInGroupsEndingWith2.Should().BeEquivalentTo(new HashSet<string> {"u3", "u2"});
            usersInGroupsEndingWith3.Should().BeEquivalentTo(new HashSet<string> {"u3"});
        }

        [Fact]
        public void ShouldRemoveGroupAfterLastUserRemoved()
        {
            _chatGroupService.RemoveUserFromGroup("u1", "g1");
            _chatGroupService.RemoveUserFromGroup("u2", "g1");
            _chatGroupService.RemoveUserFromGroup("u3", "g1");

            _chatGroupService.GetGroups(_ => true).Should().BeEquivalentTo(new HashSet<string> {"g3", "g2"});
            
            _chatGroupService.RemoveUserFromGroup("u1", "g2");
            _chatGroupService.RemoveUserFromGroup("u2", "g2");
            _chatGroupService.RemoveUserFromGroup("u3", "g2");

            _chatGroupService.GetGroups(_ => true).Should().BeEquivalentTo(new HashSet<string> {"g3"});

            _chatGroupService.RemoveUserFromGroup("u1", "g3");
            _chatGroupService.RemoveUserFromGroup("u2", "g3");
            _chatGroupService.RemoveUserFromGroup("u3", "g3");

            _chatGroupService.GetGroups(_ => true).Should().BeEmpty();
        }

        [Fact]
        public void ShouldGenericScenarioPass()
        {
            _chatGroupService.AddUserToGroup("u3", "g4");
            _chatGroupService.RemoveUserFromGroup("u3", "g3");

            _chatGroupService.GetGroups(_ => true).Should().BeEquivalentTo(new HashSet<string> {"g4", "g1", "g2"});
            _chatGroupService.GetUsers(_ => true).Should().BeEquivalentTo(new HashSet<string> {"u3", "u2", "u1"});
            
            _chatGroupService.RemoveUserFromGroup("u3", "g4");
            _chatGroupService.RemoveUserFromGroup("u3", "g1");
            _chatGroupService.RemoveUserFromGroup("u3", "g2");
            _chatGroupService.GetGroups(_ => true).Should().BeEquivalentTo(new HashSet<string> {"g1", "g2"});
            _chatGroupService.GetUsers(_ => true).Should().BeEquivalentTo(new HashSet<string> {"u2", "u1"});
        }
    }
}