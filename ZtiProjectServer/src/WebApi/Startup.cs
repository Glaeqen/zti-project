﻿using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ZtiProjectServer.Application.Operations.SignIn;
using ZtiProjectServer.Application.Operations.SignUp;
using ZtiProjectServer.Application.Services;
using ZtiProjectServer.Contracts.Models;
using ZtiProjectServer.Contracts.Repositories;
using ZtiProjectServer.Contracts.Services;
using ZtiProjectServer.Infrastructure.Repositories;
using ZtiProjectServer.Infrastructure.Security.Jwt;
using ZtiProjectServer.Persistence.Contexts;
using ZtiProjectServer.WebApi.Hubs;
using ZtiProjectServer.WebApi.Mappers;
using ZtiProjectServer.WebApi.Middlewares;

namespace ZtiProjectServer.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public string HubRoute { get; } = "/api/chat";

        public string CorsPolicy { get; } = "CorsPolicy";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy(CorsPolicy,
                    builder =>
                        builder.WithOrigins(Configuration.GetSection("ClientUrls").Get<string[]>())
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials()
                )
            );
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<ElephantSqlContext>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("ElephantSql")));
            services.AddTransient<IRepository<User>, UserRepository>();
            services.AddTransient<SignInExecutor>();
            services.AddTransient<SignUpExecutor>();
            services.AddSingleton<IChatGroupService, ChatGroupService>();
            services.AddSingleton<SecretProvider>();
            services.AddSingleton<JwtProvider>();
            services.AddSingleton<JwtValidator>();
            services.AddAutoMapper(typeof(AutoMapperProfile));

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(
                                new SecretProvider(Configuration)
                                    .Secret
                            )
                        ),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // If the request is for our hub...
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                (path.StartsWithSegments(HubRoute)))
                            {
                                // Read the token out of the query string
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseServiceExceptionHandler();
            app.UseCors(CorsPolicy);
            app.UseAuthentication();
            app.UseSignalR(routes => routes.MapHub<ChatHub>(HubRoute));
            app.UseMvc();
        }
    }
}