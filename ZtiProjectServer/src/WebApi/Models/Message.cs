namespace ZtiProjectServer.WebApi.Models
{
    public class Message
    {
        public string User { get; set; }
        public string Body { get; set; }
    }
}