using ZtiProjectServer.Application.Operations.SignIn;

namespace ZtiProjectServer.WebApi.Models
{
    public class SignInResponse
    {
        public string AccessToken { get; set; }
    }
}