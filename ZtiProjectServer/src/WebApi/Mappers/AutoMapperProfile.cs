using AutoMapper;
using ZtiProjectServer.Contracts.Models;
using ZtiProjectServer.Persistence.Models;

namespace ZtiProjectServer.WebApi.Mappers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserEntity>()
                .ForMember(userEntity => userEntity.Hashedpassword,
                    opt => opt.MapFrom(user => user.HashedPassword))
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<UserEntity, User>()
                .ForMember(user => user.HashedPassword,
                    opt => opt.MapFrom(userEntity => userEntity.Hashedpassword))
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));
        }
    }
}