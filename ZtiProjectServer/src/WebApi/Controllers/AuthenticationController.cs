﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ZtiProjectServer.Application.Operations.SignIn;
using ZtiProjectServer.Application.Operations.SignUp;
using ZtiProjectServer.WebApi.Models;

namespace ZtiProjectServer.WebApi.Controllers
{
    [Authorize]
    [Route("api/auth")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly SignInExecutor _signInExecutor;
        private readonly SignUpExecutor _signUpExecutor;

        public AuthenticationController(
            SignInExecutor signInExecutor,
            SignUpExecutor signUpExecutor
        )
        {
            _signInExecutor = signInExecutor;
            _signUpExecutor = signUpExecutor;
        }

        [AllowAnonymous]
        [HttpPost("sign-in")]
        public ActionResult<SignInResponse> SignIn([FromBody] SignInData signInData)
        {
            return Ok(_signInExecutor.Execute(signInData));
        }

        [AllowAnonymous]
        [HttpPost("sign-up")]
        public ActionResult<SignUpResponse> SignUp([FromBody] SignUpData signUpData)
        {
            return Ok(_signUpExecutor.Execute(signUpData));
        }

        [HttpGet]
        public ActionResult AuthHealthCheck()
        {
            return Ok();
        }
    }
}