using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using ZtiProjectServer.Contracts.Services;
using ZtiProjectServer.WebApi.Models;

namespace ZtiProjectServer.WebApi.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly ILogger _logger;
        private readonly IChatGroupService _chatGroupService;

        public ChatHub(ILogger<ChatHub> logger, IChatGroupService chatGroupService)
        {
            _logger = logger;
            _chatGroupService = chatGroupService;
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();

            foreach (var group in _chatGroupService.GetGroups(user => user == Context.User.Identity.Name))
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, group);
            }
            
            _logger.LogInformation($"Connected:    Context.ConnectionId: {Context?.ConnectionId}\n      " +
                                   $"Connected:    Context.UserIdentifier: {Context?.UserIdentifier}\n      " +
                                   $"Connected:    Context.User.Identity.Name: {Context?.User?.Identity?.Name}\n      " +
                                   $"Connected:    Context.User.Identity.IsAuthenticated: {Context?.User?.Identity?.IsAuthenticated}\n      " +
                                   $"Connected:    Context.User.Identity.AuthenticationType: {Context?.User?.Identity?.AuthenticationType}");
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            _logger.LogInformation($"Disconnected: Context.ConnectionId: {Context?.ConnectionId}\n      " +
                                   $"Disconnected: Context.UserIdentifier: {Context?.UserIdentifier}\n      " +
                                   $"Disconnected: Context.User.Identity.Name: {Context?.User?.Identity?.Name}\n      " +
                                   $"Disconnected: Context.User.Identity.IsAuthenticated: {Context?.User?.Identity?.IsAuthenticated}\n      " +
                                   $"Disconnected: Context.User.Identity.AuthenticationType: {Context?.User?.Identity?.AuthenticationType}\n      " +
                                   $"Disconnected: Exception: {exception?.Message}");
            await base.OnDisconnectedAsync(exception);
        }

        public IEnumerable<string> GetAllGroups()
        {
            return _chatGroupService.GetGroups(_ => true);
        }

        public IEnumerable<string> GetGroupsForUser(string user)
        {
            return _chatGroupService.GetGroups(userInGroup => userInGroup == user);
        }

        public IEnumerable<string> GetAllUsers()
        {
            return _chatGroupService.GetUsers(_ => true);
        }

        public IEnumerable<string> GetUsersForGroup(string group)
        {
            return _chatGroupService.GetUsers(groupWithUsers => groupWithUsers == group);
        }

        public async Task AddMeToGroup(string group)
        {
            _chatGroupService.AddUserToGroup(Context.User.Identity.Name, group);
            await Groups.AddToGroupAsync(Context.ConnectionId, group);
            await Clients.User(Context.User.Identity.Name).SendAsync("SubscriptionsChangedEvent");
            await Clients.All.SendAsync("GroupsChangedEvent");
        }

        public async Task RemoveMeFromGroup(string group)
        {
            _chatGroupService.RemoveUserFromGroup(Context.User.Identity.Name, group);
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, group);
            await Clients.User(Context.User.Identity.Name).SendAsync("SubscriptionsChangedEvent");
            await Clients.All.SendAsync("GroupsChangedEvent");
        }

        public async Task SendMessageToGroup(Message message, string group)
        {
            await Clients.Group(group).SendAsync("ReceiveMessageForGroup", message, group);
        }
    }
}