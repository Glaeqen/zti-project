using Microsoft.AspNetCore.Builder;

namespace ZtiProjectServer.WebApi.Middlewares
{
    public static class ServiceExceptionHandlerApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseServiceExceptionHandler(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.UseMiddleware<ServiceExceptionHandlerMiddleware>();
        }
    }
}