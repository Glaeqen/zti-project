using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ZtiProjectServer.Contracts.Exceptions;

namespace ZtiProjectServer.WebApi.Middlewares
{
    public class ServiceExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ServiceExceptionHandlerMiddleware(
            RequestDelegate next,
            ILogger<ServiceExceptionHandlerMiddleware> logger
        )
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                var httpStatusCode = (exception is ServiceException serviceException)
                    ? serviceException.StatusCode
                    : HttpStatusCode.InternalServerError;
                var result = JsonConvert.SerializeObject(new
                {
                    exception.Message
                });
                _logger.LogError($"Exception logged: {exception}");
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int) httpStatusCode;
                await context.Response.WriteAsync(result);
            }
        }
    }
}