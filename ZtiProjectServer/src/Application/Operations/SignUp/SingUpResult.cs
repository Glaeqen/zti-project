using ZtiProjectServer.Concepts.Executor;

namespace ZtiProjectServer.Application.Operations.SignUp
{
    public class SignUpResult : IResult
    {
        public string Message { get; set; }
    }
}