using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using ZtiProjectServer.Infrastructure.Security.Cryptography;
using ZtiProjectServer.Concepts.Executor;
using ZtiProjectServer.Contracts.Models;
using ZtiProjectServer.Contracts.Repositories;
using ZtiProjectServer.Infrastructure.Repositories;
using ZtiProjectServer.Infrastructure.Security.Jwt;

namespace ZtiProjectServer.Application.Operations.SignUp
{
    public class SignUpExecutor : IExecutor<SignUpData, SignUpResult>
    {
        private readonly IRepository<User> _repository;
        private readonly JwtProvider _jwtProvider;

        public SignUpExecutor(IRepository<User> repository, JwtProvider jwtProvider,
            ILogger<SignUpExecutor> logger)
        {
            _repository = repository;
            _jwtProvider = jwtProvider;
        }


        public SignUpResult Execute(SignUpData signUpData)
        {
            _repository.Add(new User
            {
                Email = signUpData.Email,
                HashedPassword = SHA256Utilities.ComputeHash(signUpData.Password),
                Username = signUpData.Username
            });

            return new SignUpResult
            {
                Message = "Signed Up",
            };
        }
    }
}