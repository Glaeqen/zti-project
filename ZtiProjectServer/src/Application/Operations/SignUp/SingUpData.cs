using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ZtiProjectServer.Concepts.Executor;
using ZtiProjectServer.Infrastructure.JsonConverters;

namespace ZtiProjectServer.Application.Operations.SignUp
{
    public class SignUpData : IData<SignUpResult>
    {
        [Required]
        [RegularExpression(@"^\S*$", ErrorMessage = "No whitespace allowed")]
        public string Username { get; set; }

        [Required]
        [RegularExpression(@"^\S*@\S*\.\S*$", ErrorMessage = "No whitespace allowed and " +
                                                             "must be valid e-mail address")]
        public string Email { get; set; }

        [Required]
        [JsonConverter(typeof(Base64Converter))]
        public string Password { get; set; }
    }
}