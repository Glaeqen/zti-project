using ZtiProjectServer.Concepts.Executor;

namespace ZtiProjectServer.Application.Operations.SignIn
{
    public class SignInResult : IResult
    {
        public string Token { get; set; }
    }
}