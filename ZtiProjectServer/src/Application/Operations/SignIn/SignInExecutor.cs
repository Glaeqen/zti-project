using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using ZtiProjectServer.Infrastructure.Security.Cryptography;
using ZtiProjectServer.Concepts.Executor;
using ZtiProjectServer.Contracts.Exceptions.Operations.SignIn;
using ZtiProjectServer.Contracts.Models;
using ZtiProjectServer.Contracts.Repositories;
using ZtiProjectServer.Infrastructure.Security.Jwt;

namespace ZtiProjectServer.Application.Operations.SignIn
{
    public class SignInExecutor : IExecutor<SignInData, SignInResult>
    {
        private readonly IRepository<User> _repository;
        private readonly JwtProvider _jwtProvider;

        public SignInExecutor(IRepository<User> repository, JwtProvider jwtProvider,
            ILogger<SignInExecutor> logger)
        {
            _repository = repository;
            _jwtProvider = jwtProvider;
        }


        public SignInResult Execute(SignInData signInData)
        {
            var amountOfMatches = _repository.Get().Count(user =>
                user.Username == signInData.Username &&
                user.HashedPassword == SHA256Utilities.ComputeHash(signInData.Password));

            switch (amountOfMatches)
            {
                case 0:
                    throw new UserIsNotAuthenticatedException("User with given credentials is not authenticated");
                case 1:
                    return new SignInResult
                    {
                        Token = _jwtProvider.GenerateToken(
                                claims: new Dictionary<string, object>
                                {
                                    [ClaimTypes.Name] = signInData.Username,
                                    [ClaimTypes.NameIdentifier] = signInData.Username,
                                    ["subject"] = signInData.Username,
                                }
                            ).Token,
                    };
                default:
                    throw new UserAuthenticationFailedException("Authentication Failed");
            }
        }
    }
}