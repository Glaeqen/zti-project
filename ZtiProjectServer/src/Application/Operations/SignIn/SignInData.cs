using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ZtiProjectServer.Concepts.Executor;
using ZtiProjectServer.Infrastructure.JsonConverters;

namespace ZtiProjectServer.Application.Operations.SignIn
{
    public class SignInData : IData<SignInResult>
    {
        [Required]
        [RegularExpression(@"^\S*$", ErrorMessage = "No whitespace allowed")]
        public string Username { get; set; }

        [Required]
        [JsonConverter(typeof(Base64Converter))]
        public string Password { get; set; }
    }
}