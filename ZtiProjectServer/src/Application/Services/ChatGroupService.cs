using System;
using System.Collections.Generic;
using System.Linq;
using ZtiProjectServer.Contracts.Services;

namespace ZtiProjectServer.Application.Services
{
    public class ChatGroupService : IChatGroupService
    {
        private readonly IDictionary<string, HashSet<string>> _groupToUsersMap =
            new Dictionary<string, HashSet<string>>();

        public void AddUserToGroup(string user, string group)
        {
            if (_groupToUsersMap.ContainsKey(group))
            {
                _groupToUsersMap[group].Add(user);
            }
            else
            {
                _groupToUsersMap[group] = new HashSet<string> {user};
            }
        }

        public void RemoveUserFromGroup(string user, string group)
        {
            if (_groupToUsersMap.ContainsKey(group))
            {
                var users = _groupToUsersMap[group];
                users.Remove(user);
                if (users.Count == 0)
                {
                    _groupToUsersMap.Remove(group);
                }
            }
        }

        public IEnumerable<string> GetGroups(Func<string, bool> predicate)
        {
            return _groupToUsersMap
                .Where(pair => pair.Value.Any(predicate))
                .Select(pair => pair.Key);
        }

        public IEnumerable<string> GetUsers(Func<string, bool> predicate)
        {
            return _groupToUsersMap
                .Where(pair => predicate(pair.Key))
                .SelectMany(pair => pair.Value)
                .Distinct();
        }
    }
}