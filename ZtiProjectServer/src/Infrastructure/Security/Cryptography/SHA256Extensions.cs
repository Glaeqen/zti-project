using System.Security.Cryptography;
using System.Text;

namespace ZtiProjectServer.Infrastructure.Security.Cryptography
{
    public static class SHA256Utilities
    {
        public static string ComputeHash(string value)
        {
            using (var sha256Instance = SHA256.Create())
            {
                return Encoding.UTF8.GetString(
                    sha256Instance.ComputeHash(
                        Encoding.UTF8.GetBytes(
                            value
                        )
                    )
                );
            }
        }
    }
}