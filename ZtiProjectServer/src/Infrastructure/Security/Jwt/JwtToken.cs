namespace ZtiProjectServer.Infrastructure.Security.Jwt
{
    public class JwtToken
    {
        public string Token { get; set; }
    }
}