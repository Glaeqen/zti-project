using System;
using JWT;
using JWT.Builder;

namespace ZtiProjectServer.Infrastructure.Security.Jwt
{
    public class JwtValidator
    {
        public enum Result
        {
            Valid,
            Expired,
            InvalidSignature,
            InvalidTokenParts
        }

        private readonly SecretProvider _secretProvider;

        public JwtValidator(SecretProvider secretProvider)
        {
            _secretProvider = secretProvider;
        }

        public Result ValidateToken(JwtToken token)
        {
            try
            {
                var _ = new JwtBuilder()
                    .WithSecret(_secretProvider.Secret)
                    .MustVerifySignature()
                    .Decode(token.Token);
            }
            catch (TokenExpiredException)
            {
                return Result.Expired;
            }
            catch (SignatureVerificationException)
            {
                return Result.InvalidSignature;
            }
            catch (InvalidTokenPartsException)
            {
                return Result.InvalidTokenParts;
            }
            catch (Exception exception)
            {
                throw new JwtTokenValidationFailedException("Unknown error occurred", exception);
            }

            return Result.Valid;
        }
    }
}