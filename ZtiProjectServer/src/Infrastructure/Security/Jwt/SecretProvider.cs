using Microsoft.Extensions.Configuration;

namespace ZtiProjectServer.Infrastructure.Security.Jwt
{
    public class SecretProvider
    {
        public SecretProvider(IConfiguration configuration)
        {
            Secret = configuration["JWTSecret"];
        }

        public string Secret { get; }
    }
}