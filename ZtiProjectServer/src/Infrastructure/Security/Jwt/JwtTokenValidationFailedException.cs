using System;

namespace ZtiProjectServer.Infrastructure.Security.Jwt
{
    public class JwtTokenValidationFailedException : Exception
    {
        public JwtTokenValidationFailedException()
        {
        }

        public JwtTokenValidationFailedException(string message) : base(message)
        {
        }

        public JwtTokenValidationFailedException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}