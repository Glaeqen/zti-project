using System;
using System.Collections.Generic;
using JWT.Algorithms;
using JWT.Builder;

namespace ZtiProjectServer.Infrastructure.Security.Jwt
{
    public class JwtProvider
    {
        private readonly SecretProvider _secretProvider;

        public JwtProvider(SecretProvider secretProvider)
        {
            _secretProvider = secretProvider;
        }

        public JwtToken GenerateToken(IDictionary<string, object> claims)
        {
            return new JwtToken
            {
                Token = new JwtBuilder()
                    .WithAlgorithm(new HMACSHA256Algorithm())
                    .WithSecret(_secretProvider.Secret)
                    .AddClaims(claims)
                    .AddClaim("exp", DateTimeOffset.UtcNow.AddDays(1).ToUnixTimeSeconds())
                    .Build()
            };
        }
    }
}