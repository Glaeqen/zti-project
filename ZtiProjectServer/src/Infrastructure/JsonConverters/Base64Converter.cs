using System;
using System.Text;
using Newtonsoft.Json;
using ZtiProjectServer.Infrastructure.Exceptions;

namespace ZtiProjectServer.Infrastructure.JsonConverters
{
    public class Base64Converter : JsonConverter<string>
    {
        public override void WriteJson(JsonWriter writer, string value, JsonSerializer serializer)
        {
            writer.WriteValue(Convert.ToBase64String(Encoding.UTF8.GetBytes(value)));
        }

        public override string ReadJson(JsonReader reader, Type objectType, string existingValue, bool hasExistingValue,
            JsonSerializer serializer)
        {
            try
            {
                return Encoding.UTF8.GetString((Convert.FromBase64String(reader.Value as string ?? "")));
            }
            catch (FormatException exception)
            {
                throw new IsNotValidBase64StringServiceException(
                    $"Passed value for '{reader.Path}' is not valid Base-64 string", exception);
            }
        }
    }
}