using System;
using System.Net;
using ZtiProjectServer.Contracts.Exceptions;

namespace ZtiProjectServer.Infrastructure.Exceptions
{
    public class IsNotValidBase64StringServiceException : ServiceException
    {
        public IsNotValidBase64StringServiceException(string message, Exception exception)
            : base(HttpStatusCode.BadRequest, message, exception)
        {
        }
    }
}