using System;
using System.Linq;
using AutoMapper;
using ZtiProjectServer.Contracts.Models;
using ZtiProjectServer.Contracts.Repositories;
using ZtiProjectServer.Persistence.Contexts;
using ZtiProjectServer.Persistence.Models;

namespace ZtiProjectServer.Infrastructure.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ElephantSqlContext _dbContext;
        private readonly IMapper _mapper;

        public UserRepository(ElephantSqlContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public IQueryable<User> Get()
        {
            return _dbContext.User.Select(entity => _mapper.Map<UserEntity, User>(entity));
        }

        public void Delete(User user)
        {
            try
            {
                var userEntity = _dbContext.User.Single(entity => entity.Id == user.Id);
                _dbContext.User.Remove(userEntity);
                _dbContext.SaveChanges();
            }
            catch (InvalidOperationException exception)
            {
                throw new UserDoesNotExistException("User with specified id does not exists", exception);
            }
        }

        public void Add(User user)
        {
            var userEntity = _mapper.Map<User, UserEntity>(user);
            if (_dbContext.User.Any(entity => entity.Id == userEntity.Id ||
                                              entity.Username == userEntity.Username ||
                                              entity.Email == userEntity.Email)
            )
            {
                throw new UserAlreadyExistsException("User with one of such parameters already exists");
            }

            _dbContext.User.Add(userEntity);
            _dbContext.SaveChanges();
        }

        public void Update(User user)
        {
            try
            {
                var userEntity = _dbContext.User.Single(entity => entity.Id == user.Id);
                _dbContext.User.Update(_mapper.Map(user, userEntity));
                _dbContext.SaveChanges();
            }
            catch (InvalidOperationException exception)
            {
                throw new UserDoesNotExistException("User with specified id does not exists", exception);
            }
        }
    }
}