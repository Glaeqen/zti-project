namespace ZtiProjectServer.Concepts.Executor
{
    public interface IExecutor<in TIn, out TOut>
        where TIn : IData<TOut>
        where TOut : IResult
    {
        TOut Execute(TIn t);
    }
}