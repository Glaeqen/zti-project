using System;

namespace ZtiProjectServer.Concepts.Monads
{
    public class Option<T> where T: class
    {
        private readonly T _instance;

        private Option(T instance)
        {
            _instance = instance;
        }

        public Option<TOut> Bind<TOut>(Func<T, Option<TOut>> some, Func<Option<TOut>> none) where TOut: class
        {
            return _instance != null ? some(_instance) : none();
        }

        public Option<TOut> Map<TOut>(Func<T, TOut> some, Func<TOut> none) where TOut : class
        {
            return _instance != null ? new Option<TOut>(some(_instance)) : new Option<TOut>(none());
        }
    }
}