using System;
using System.Net;

namespace ZtiProjectServer.Contracts.Exceptions.Operations.SignIn
{
    public class UserAuthenticationFailedException : ServiceException
    {
        public UserAuthenticationFailedException(string message)
            :base(HttpStatusCode.InternalServerError, message)
        {
        }
        
        public UserAuthenticationFailedException(string message, Exception exception)
            :base(HttpStatusCode.InternalServerError, message, exception)
        {
        }
    }
}