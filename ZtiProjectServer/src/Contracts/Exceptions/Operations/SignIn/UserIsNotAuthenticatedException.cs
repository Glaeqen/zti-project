using System;
using System.Net;

namespace ZtiProjectServer.Contracts.Exceptions.Operations.SignIn
{
    public class UserIsNotAuthenticatedException : ServiceException
    {
        public UserIsNotAuthenticatedException(string message)
            :base(HttpStatusCode.Unauthorized, message)
        {
        }
        
        public UserIsNotAuthenticatedException(string message, Exception exception)
            :base(HttpStatusCode.Unauthorized, message, exception)
        {
        }
    }
}