using System;
using System.Net;

namespace ZtiProjectServer.Contracts.Exceptions
{
    public abstract class ServiceException : Exception
    {
        protected ServiceException(HttpStatusCode statusCode, string message, Exception nestedException)
            : base(message, nestedException)
        {
            StatusCode = statusCode;
        }

        protected ServiceException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }
    }
}