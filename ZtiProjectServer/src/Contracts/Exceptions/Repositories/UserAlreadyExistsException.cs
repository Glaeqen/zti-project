using System;
using System.Net;
using ZtiProjectServer.Contracts.Exceptions;

namespace ZtiProjectServer.Infrastructure.Repositories
{
    public class UserAlreadyExistsException : ServiceException
    {
        public UserAlreadyExistsException(string message)
            : base(HttpStatusCode.Conflict, message)
        {
        }

        public UserAlreadyExistsException(string message, Exception nestedException)
            : base(HttpStatusCode.Conflict, message, nestedException)
        {
        }
    }
}