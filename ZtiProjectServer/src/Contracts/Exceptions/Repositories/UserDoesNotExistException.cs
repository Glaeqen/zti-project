using System;
using System.Net;
using ZtiProjectServer.Contracts.Exceptions;

namespace ZtiProjectServer.Infrastructure.Repositories
{
    public class UserDoesNotExistException : ServiceException
    {
        public UserDoesNotExistException(string message)
            : base(HttpStatusCode.NotFound, message)
        {
        }
        public UserDoesNotExistException(string message, Exception exception)
            : base(HttpStatusCode.NotFound, message, exception)
        {
        }
    }
}