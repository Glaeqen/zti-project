using System.Collections.Generic;
using System;

namespace ZtiProjectServer.Contracts.Services
{
    public interface IChatGroupService
    {
        void AddUserToGroup(string user, string group);
        void RemoveUserFromGroup(string user, string group);
        IEnumerable<string> GetGroups(Func<string, bool> predicate);
        IEnumerable<string> GetUsers(Func<string, bool> predicate);
    }
}