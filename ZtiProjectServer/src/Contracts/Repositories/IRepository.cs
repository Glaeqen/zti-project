using System.Linq;

namespace ZtiProjectServer.Contracts.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> Get();
        void Delete(T t);
        void Add(T t);
        void Update(T t);
    }
}