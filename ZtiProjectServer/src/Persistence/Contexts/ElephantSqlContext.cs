using Microsoft.EntityFrameworkCore;
using ZtiProjectServer.Persistence.Models;

namespace ZtiProjectServer.Persistence.Contexts
{
    public partial class ElephantSqlContext : DbContext
    {
        public ElephantSqlContext()
        {
        }

        public ElephantSqlContext(DbContextOptions<ElephantSqlContext> options)
            : base(options)
        {
        }

        public virtual DbSet<UserEntity> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>(entity =>
            {
                entity.ToTable("user");

                entity.HasIndex(e => e.Email)
                    .HasName("user_email_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("user_username_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.Hashedpassword)
                    .IsRequired()
                    .HasColumnName("hashedpassword")
                    .HasMaxLength(255);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(255);
            });
        }
    }
}