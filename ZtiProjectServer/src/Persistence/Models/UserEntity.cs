namespace ZtiProjectServer.Persistence.Models
{
    public partial class UserEntity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Hashedpassword { get; set; }
    }
}
